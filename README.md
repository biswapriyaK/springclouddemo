## Steps to run this project
--

1. Open git bash where you want to check out the projcet.
2. Type command "git clone https://biswapriyaK@bitbucket.org/biswapriyaK/springclouddemo.git".
3. Go to the root folder of the project.
4. Build the project using command "mvn clean install"
5. Run on docker using the command "docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build"

--

## To test the project if it has been executed properly follow the steps mentioned below:

1.	Open the url "http://localhost:8761" and see if the eureka console is coming.
2. 	Open any rest client and make a get request on the url "http://localhost:8010/employeeDetails/111".
